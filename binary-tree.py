from collections import deque


class TreeNode():
    """
    TreeNode class represents binary tree node
    """
    def __init__(self, value=None):
        self.value = value
        self.left = None
        self.right = None

    def __repr__(self):
        """
        It is a good practice to represent define repr as something
        that can recreate the object, i.e. we can recreate this object
        from return value of repr function
        """
        return f'TreeNode({self.value}, {self.left}, {self.right})'

    def __str__(self):
        return f'{self.value}'


class BinaryTree():
    """
    Represents a binary tree and methods on them.
    TODO:
    We want to keep this binary tree as balanced as possible; we
    insert nodes in it with this goal in mind
    """
    def __init__(self, root=None):
        self.root = root

    def insert(self, node):
        #TODO
        root_node = self.root

    def iterative_in_order(self):
        nodes_in_order = []
        current = self.root
        stack = []
        while stack or current:
            if current:
                print("current", current)
                stack.append(current)
                current = current.left
            else:
                current = stack.pop()
                nodes_in_order.append(current)
                current = current.right
        return nodes_in_order

    def iterative_pre_order(self):
        nodes_in_pre_order = []
        current = self.root
        # important
        if not current:
            return nodes_in_pre_order
        stack = [current] # for pre-order, we need to print the root first
        # so we will just pop and add it in our result
        while stack:
            current = stack.pop()
            nodes_in_pre_order.append(current)
            # push right child first so that left is popped before it
            if current.right:
                stack.append(current.right)
            if current.left:
                stack.append(current.left)
        return nodes_in_pre_order

    def iterative_post_order(self):
        nodes_in_post_order = []
        current = self.root
        stack = []
        last_visited_node = None
        while current or stack:
            if current:
                stack.append(current)
                current = current.left
            else:
                peek_node = stack[-1]
                # following is a very subtle and important condition,
                # you'll have to work out an example yourself to see why we
                # do this to avoid infinite loop by not going to right child
                # again
                if peek_node.right and peek_node.right != last_visited_node:
                    current = peek_node.right
                else:
                    last_visited_node = stack.pop()
                    nodes_in_post_order.append(last_visited_node)
        return nodes_in_post_order

    def level_order(self):
        nodes_in_level_order = []
        current = self.root
        if not current: return nodes_in_level_order
        # A deque is a double ended queue with O(1) push and pop operation
        # from either side
        queue = deque()
        queue.append(current)
        while queue:
            current = queue.popleft() # we use append(), which appends to right
            # and popleft() which pops from left, giving us first in, first out
            # order. Alternatively, we can also use appendleft() and pop() which
            # appends to left (duh!) and pops from right
            nodes_in_level_order.append(current)
            children = []
            if current.left:
                children.append(current.left)
            if current.right:
                children.append(current.right)
            queue.extend(children)
        return nodes_in_level_order

    def morris_in_order_traversal(self):
        # https://en.wikipedia.org/wiki/Threaded_binary_tree
        # All the previous traversals use extra space (stack or deque)
        # For systems with limited memory, it might be useful to traverse
        # tree without extra space
        nodes_in_order = []
        current = self.root
        if not current: return nodes_in_order
        while current:
            if current.left:
                pre = current.left
                while pre.right and pre.right != current:
                    pre = pre.right

                if not pre.right:
                    pre.right = current
                    current = current.left
                else:
                    nodes_in_order.append(current)
                    current = current.right
                    pre.right = None
            else:
                nodes_in_order.append(current)
                current = current.right
        return nodes_in_order


if __name__ == "__main__":
    a = TreeNode('a')
    b = TreeNode('b')
    c = TreeNode('c')
    d = TreeNode('d')
    e = TreeNode('e')
    f = TreeNode('f')
    g = TreeNode('g')

    b.left = d
    b.right = e

    c.left = f
    c.right = g

    a.left = b
    a.right = c
    tree = BinaryTree(a)

    # print("In order", tree.iterative_in_order())
    # print("Pre order", tree.iterative_pre_order())
    # print("Post order", tree.iterative_post_order())
    # print("Level order", tree.level_order())
    print("Morris in-order", tree.morris_traversal())
    # print(tree)